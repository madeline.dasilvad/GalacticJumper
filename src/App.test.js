import { act } from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders HES-SO title', () => {
  act(() => {
    render(<App />);
  });
  const titleElement = screen.getByText(/HES-SO Vs - 64-31 - HTML\/CSS\/JavaScript/i);
  expect(titleElement).toBeInTheDocument();
});
