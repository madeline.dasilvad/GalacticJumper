import React from 'react';
import { NavLink } from 'react-router-dom';

function MenuLinks() {
  const links = [
    { to: '/description', label: 'Description' },
    { to: '/sketch', label: 'Sketch' },
    { to: '/mockup', label: 'Mockup' },
    { to: '/flow', label: 'Flow' },
    { to: '/skills', label: 'Skills' },
    { to: '/logbook', label: 'Logbook' },
    { to: 'https://galactic-jumper-game.vercel.app/', label: 'Game', external: true }
  ];

  const renderLink = ({ to, label, external }) => {
    if (external) {
      return (
        <li key={to}>
          <a href={to} target="_blank" rel="noopener noreferrer" className="external-link">
            {label}
          </a>
        </li>
      );
    }
    return (
      <NavLink to={to} key={to} className={({ isActive }) => (isActive ? 'active' : '')}>
        {({ isActive }) => (
          <li className={isActive ? 'active' : ''}>
            {label}
          </li>
        )}
      </NavLink>
    );
  };

  return (
    <ul>
      {links.map(renderLink)}
    </ul>
  );
}

export default MenuLinks;
