import React from 'react';
import './ressources/css/style.css';

function Footer() {
  return (
    <footer>
      <img id="logo" src="./ressources/images/logo.png" alt="Logo" />
    </footer>
  );
}

export default Footer;
