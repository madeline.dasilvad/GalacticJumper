import React, { useState } from 'react';
import MenuLinks from './MenuLinks';

function Navigation() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const displayMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <nav>
      <ul>
        <li className="hamburger">
          <button onClick={displayMenu} aria-label="Menu for mobile">
            <img
              src="ressources/images/hamburger_icon.svg"
              alt="Menu for mobile"
              className={isMenuOpen ? 'rotate' : ''}
            />
          </button>
        </li>
        <div id="menu-links" className={`menu-links ${isMenuOpen ? 'responsive' : ''}`}>
          <MenuLinks />
        </div>
      </ul>
    </nav>
  );
}

export default Navigation;
