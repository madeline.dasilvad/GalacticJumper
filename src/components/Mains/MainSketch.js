import React from 'react';
import ArticleSketch from './Articles/ArticleSketch';

function MainSketch() {
  return (
    <main>
      <section id="articles">
        <ArticleSketch />
      </section>
    </main>
  );
}

export default MainSketch;
