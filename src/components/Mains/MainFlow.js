import React from 'react';
import ArticleFlow from './Articles/ArticleFlow';

function MainFlow() {
  return (
    <main>
      <section id="articles">
        <ArticleFlow />
      </section>
    </main>
  );
}

export default MainFlow;
