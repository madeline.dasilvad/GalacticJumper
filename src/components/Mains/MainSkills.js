import React from 'react';
import ArticleSkills from './Articles/ArticleSkills';

function MainSkills() {
  return (
    <main>
      <section id="articles">
        <ArticleSkills />
      </section>
    </main>
  );
}

export default MainSkills;
