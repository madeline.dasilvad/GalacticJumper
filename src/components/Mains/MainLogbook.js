import React from 'react';
import ArticleLogbook from './Articles/ArticleLogbook';
import AsideLogbook from './Asides/AsideLogbook';

function MainLogbook() {
  return (
    <main>
      <section id="articles">
        <ArticleLogbook />
      </section>
      <AsideLogbook />
    </main>
  );
}

export default MainLogbook;
