import React, { useEffect, useState } from 'react';
import axios from 'axios';
import HeaderP from './HeaderP';

function ArticleFlow() {
    const [page, setPage] = useState(null); 

    useEffect(() => {
        
        axios.get('https://dev-galacticjumpercc.pantheonsite.io/wp-json/wp/v2/pages/16')
            .then(response => {
                setPage(response.data); 
            })
            .catch(error => console.log(error)); 
    }, []);

    return (
        <div>
            {page ? (
                <article>
                    <h2>{page.title.rendered}</h2>
                    <HeaderP />
                    <div dangerouslySetInnerHTML={{ __html: page.content.rendered }} />
                </article>
            ) : (
                <p>Loading page...</p>
            )}
        </div>
    );
}

export default ArticleFlow;
