import React from 'react';

function AsideDescription() {
  return (
    <aside>
      <div>
        <h3>Datasheet</h3>
        <ul>
          <li>
            <p><strong>Nicole Paseri :</strong> Game design & coding</p>
          </li>
          <li>
            <p><strong>Kevin Ferreira :</strong> Game architecture & coding</p>
          </li>
          <li>
            <p><strong>Madeline da Silva Dias :</strong> CSS styling & coding</p>
          </li>
        </ul>
      </div>
    </aside>
  );
}

export default AsideDescription;
