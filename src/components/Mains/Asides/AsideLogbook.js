import React from 'react';

function AsideLogbook() {
  return (
    <aside>
        <div>
            <h3>Summary<br />of project data</h3>
            <ul>
                <li>
                    <p><strong>Total time of project</strong>: 115 hours</p>
                </li>
                <li>
                    <p><strong>Pourcentage of fun</strong>: 80%</p>
                </li>
                <li>
                    <p><strong>Pourcentage of trouble</strong>: 30%</p>
                </li>
                <li>
                    <p><strong>Pourcentage of learning</strong>: 50%</p>
                </li>
            </ul>
        </div>
    </aside>
  );
}

export default AsideLogbook;
