import React from 'react';
import ArticleMockup from './Articles/ArticleMockup';

function MainMockup() {
  return (
    <main>
      <section id="articles">
        <ArticleMockup />
      </section>
    </main>
  );
}

export default MainMockup;
