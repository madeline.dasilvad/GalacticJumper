// MainDescription.js
import React from 'react';
import ArticleDescription from './Articles/ArticleDescription';
import AsideDescription from './Asides/AsideDescription';

function MainDescription() {
  return (
    <main>
      <section id="articles">
        <ArticleDescription />
      </section>
      <AsideDescription />
    </main>
  );
}

export default MainDescription;
