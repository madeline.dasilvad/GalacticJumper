import React from 'react';
import Navigation from './Navigation';
import './ressources/css/style.css';

function Header() {
  return (
    <header>
      <h1>HES-SO Vs - 64-31 - HTML/CSS/JavaScript</h1>
      <Navigation />
    </header>
  );
}

export default Header;
