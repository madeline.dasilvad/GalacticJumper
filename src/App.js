import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Header from './components/Header';
import MainDescription from './components/Mains/MainDescription';
import MainFlow from './components/Mains/MainFlow';
import MainSketch from './components/Mains/MainSketch';
import MainMockup from './components/Mains/MainMockup';
import MainSkills from './components/Mains/MainSkills';
import MainLogbook from './components/Mains/MainLogbook';
import Footer from './components/Footer';

function App() {
  return (
    <Router>
      <Header />
      <Routes>
        <Route path="/description" element={<MainDescription />} />
        <Route path="/flow" element={<MainFlow />} />
        <Route path="/sketch" element={<MainSketch />} />
        <Route path="/mockup" element={<MainMockup />} />
        <Route path="/skills" element={<MainSkills />} />
        <Route path="/logbook" element={<MainLogbook />} />
        <Route path="/" element={<Navigate to="/description" />} /> {/* Default route */}
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
